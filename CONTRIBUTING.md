# Contributing to Floodlight's Motion Backgrounds
All contributions must follow a few rules:

- All contributions must adhere to the design principles listed
[here](README.md#design-principles). They will be applied very strictly, and
you will probably be asked to make changes before your work is accepted.
- All contributions must be your own work. You may not submit material that was
created by someone else, even if you are certain it is public domain.
- All contributions must have an associated Blender source file. This ensures
that it can be freely modified, updated, or re-rendered in the future.
- By contributing, you **irrevocably** dedicate all submitted material to the
public domain, under the CC0 public domain dedication.

## How to Submit Your Work
- Sign up for a free account at [gitlab.com](https://gitlab.com).
- In the sidebar of this project, click on "Issues", then click the green
"New Issue" button.
- Attach or include the following in the issue:
  - The background, rendered in at least 720p. If you can't fit the video in an
  attachment, a YouTube link is fine. Don't worry about video quality; I will
  re-render all submissions.
  - The Blender source file
- Click "Submit Issue". Remember that issues are public.
- I will reply to your issue with any clarifications or needed improvements.
When everything is ready, I will add your submission to the repository and to
Floodlight's website.
