# Repository Maintenance
This file is a reminder to myself and any future maintainers of the repository.
It describes how everything works, because I am likely to forget, and if I get
hit by a meteorite someone needs to know how to run things.

## Contributions
Contributions are made through issues, NOT merge requests, because it is a bit
complicated, and Blender users might not be familiar with git. See
[CONTRIBUTING.md](CONTRIBUTING.md), which describes the process for potential
contributors.

## File Structure
Normally, generated files are not committed to version control. However, these
backgrounds take hours each to render, so it is necessary to include them.
Individual frames, however, are excluded by .gitignore.

Each background has its own directory (particles-1, lightrays-1, etc.) There
should be three items in this directory:
- The Blender file (e.g. particles-1.blend)
- The finished output (e.g. particles-1.mov)
- A directory containing the frames of the video. Only the first frame is
committed, to be used as a thumbnail; the rest are `.gitignore`'d.

## Creating Videos From Frames
Blender files should be set to output frames as individual images. Do this by
setting the output destination to `//frames/`.

`compile.sh` is a Bash script used to create the video file from those frames.
Simply run `compile.sh <name of background>` and it will generate the video.

The script requires ffmpeg. On macOS, you will need to install it using
[Homebrew](https://brew.sh): `brew install ffmpeg`

## Video Resolution
All videos should be rendered in 1080p, 16:9 aspect ratio. This is Blender's
default, but you must remember to set the percentage to 100%. It defaults to
50% to save time.

## Continuous Integration & GitLab Pages
GitLab's CI and Pages features are used to publish the backgrounds on the web.
The CI script will automatically create a zip file for the videos, a zip file
for the sources, and small (320x180) thumbnails. These, plus the individual
videos and full-size thumbnails, are copied to GitLab Pages. You can view the
file structure by looking at the artifacts of the latest pipeline.