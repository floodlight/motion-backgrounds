#!/bin/bash

if [ "$#" -ne 1 ] ; then
  echo "usage: compile.sh <subdir>"
  exit 1
fi

ffmpeg -framerate 24 -y -i $1/frames/%04d.png -pix_fmt yuv420p $1/$1.mov
