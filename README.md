# Motion Backgrounds
A collection of animated backgrounds for use with Floodlight Presenter (or
other lyrics presentation software).

## Gallery
<a href="lightrays-1/lightrays-1.mov">
  <img width="320" height="180" src="lightrays-1/frames/0001.png"/>
</a>
<a href="particles-1/particles-1.mov">
  <img width="320" height="180" src="particles-1/frames/0001.png"/>
</a>
<a href="shapes-1/shapes-1.mov">
  <img width="320" height="180" src="shapes-1/frames/0001.png"/>
</a>

## Design Principles
As with all Floodlight projects, the goal is to minimize distractions and put
the focus on worship. Thus, there are a few design principles that all
backgrounds must follow:

- **Subtlety.** Anything flashy or distracting is unsuitable for this
collection. Motion backgrounds exist to fill the space behind the words on the
screen. They are not the end goal.

    If you want to use a background because it looks cool, you are probably not
using them for the right purpose.

- **Perfect loops.** Presenter does not support "soft loop" functionality, so
all backgrounds made for it must loop perfectly. Otherwise, the transition back
to the beginning of the animation will be jarring. (This can happen even with
soft loop, if the background is poorly designed).

- **Legibility.** Usually, these backgrounds will be placed under white text.
The words *must* be very clearly legible. For this reason, most of the
backgrounds are fairly dark.

## Contributing
If you have some skill with [Blender](https://www.blender.org/) and would like
to contribute, great! Please read [this guide](CONTRIBUTING.md), which will
explain how to contribute.

## License
![Public Domain](https://licensebuttons.net/p/zero/1.0/88x31.png)

The contents of this repository are in the public domain. This means you may
use them for any purpose without restriction. See the [CC0 Public Domain
Dedication](http://creativecommons.org/publicdomain/zero/1.0/) for more
information.
